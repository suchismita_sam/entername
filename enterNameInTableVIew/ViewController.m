//
//  ViewController.m
//  enterNameInTableVIew
//
//  Created by Click Labs134 on 9/28/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"
NSMutableArray *nameArray;
UIImageView *backgroundImage;

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UITableView *currentCell;
@property (strong, nonatomic) IBOutlet UITextField *enterNameTextFIeld;
@property (strong, nonatomic) IBOutlet UIButton *addButton;

@end

@implementation ViewController
@synthesize currentCell;
@synthesize enterNameTextFIeld;
@synthesize addButton;
- (void)viewDidLoad {
    [super viewDidLoad];

    
    nameArray=[[NSMutableArray alloc]init];
    
    [addButton addTarget:self action:@selector(add:) forControlEvents:UIControlEventTouchUpInside];
 
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void) add:(UIButton *)sender
{
    [nameArray addObject:enterNameTextFIeld.text];
    [currentCell reloadData];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return nameArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cellReuse"];
    cell.textLabel.text=nameArray[indexPath.row];
    cell.textColor=[UIColor blueColor];
    return cell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
